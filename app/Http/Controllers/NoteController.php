<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\User;
use App\Page;
use App\Subpage;
use App\Note;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NoteController extends Controller
{
    public function index($userId, $pageId){
		return Page::with( array( 'notes' ) )->where('id', $pageId)->get();
	}

	public function show($userId, $pageId, $noteId){
		return Note::find($noteId)->get();
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  int  $id
	* @return Response
	*/
	public function update($userId, $pageId, $noteId)
	{

		$note = Note::find($noteId);

		$note->content  = Input::get('content');
		$note->shape = Input::get('shape');
		$note->page_id   = Input::get('page_id');
		$note->important   = Input::get('important');
		$note->ended   = Input::get('ended');


		if($note->save())
		{
			return json_decode(true);
		}
		else
		{
			return json_decode(false);
		}
	}

  /**
  * Store resource in storage.
  *
  * @param  int  $id
  * @return Response
  */
  public function saveNote()
  {
    $note = new Note;

    $note->name  = Input::get('name');
    $note->content  = Input::get('content');
		$note->shape = Input::get('shape');
		$note->page_id   = Input::get('page_id');
		$note->important   = Input::get('important');
		$note->ended   = Input::get('ended');


		if($note->save())
		{
			return json_decode($note->id);
		}
		else
		{
			return json_decode(false);
		}
  }

}
