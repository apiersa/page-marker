<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WwwController extends Controller
{
    public function index(){
      $addr = Input::get('addr');

      $homepage = file_get_contents($addr);

      $homepage = preg_replace("/<a\s[^>]*href=(\"??)(http[^\" >]*?)\\1[^>]*>(.*)<\/a>/siU", "elo", $homepage);
      file_put_contents('sites/cache_page.html', $homepage);

      return $addr;
    }
}
