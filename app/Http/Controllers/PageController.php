<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Page;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class PageController extends Controller
{
    
	public function index($userId){
		$pages = Page::with( array('notes') )
					->where('user_id', $userId)
					->get();
		return $pages;
	}
	
	 public function show($userId, $pageId){
		return Page::find($pageId);
	}
}
;