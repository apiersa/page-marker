<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index(){
		return User::selectRaw('*, CONCAT(imie, " ", nazwisko) as full_name')
          ->orderBy('nazwisko')
          ->get();
	}

	public function show($id){
		return User::find($id);
	}
}
