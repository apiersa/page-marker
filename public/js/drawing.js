initDraw(document.getElementById('main_canvas'));

var add_figure_to_note_mode = false;

/*
* Funkcja odswiezajaca polozenie notek przy scrollowaniu strony www
* id - ID elementu scrollowanego
*
* last_scroll_position - zmienna pamietajaca poprzednie polozenie suwaka, potrzebna do obliczen
*/

var last_scroll_position = 0;

function redraw(scrollTop) {
	$('#main_canvas').offset({top: 0 - scrollTop});
	$('#main_canvas').height($('#main_canvas').height() + scrollTop - last_scroll_position);
	last_scroll_position = scrollTop;
}

//zmienna globalna drawing_mode (zezwalajaca na rysowanie)
var drawing_mode = 0;
//zmienna globalna shape_type (rodzaj rysowanej figury (mozliwe -> rectangle i circle))
var shape_type = 'rectangle';

function initDraw(canvas) {

		var box = { left: 0, top: 0 };
        try {
            box = canvas.getBoundingClientRect();
        }
        catch(e)
        { console.log(e); }

	function setMousePosition(e) {
        var ev = e || window.event; //Moz || IE
        if (ev.pageX) { //Moz
            mouse.x = ev.pageX - box.left; //??
            mouse.y = ev.pageY - box.top; //??
        } else if (ev.clientX) { //IE
            mouse.x = ev.clientX + document.body.scrollLeft;
            mouse.y = ev.clientY + document.body.scrollTop;
        }

    };

    var mouse = {
        x: 0,
        y: 0,
        startX: 0,
        startY: 0
    };
    var element = null;

    canvas.onmousemove = function (e) {
        setMousePosition(e);
		if (element !== null) {
            element.style.width = Math.abs(mouse.x - mouse.startX) + 'px';
            element.style.height = Math.abs(mouse.y - mouse.startY + global_scrollTop) + 'px';
            element.style.left = (mouse.x - mouse.startX < 0) ? mouse.x + 'px' : mouse.startX + 'px';
            element.style.top = (mouse.y + global_scrollTop - mouse.startY < 0) ? mouse.y + global_scrollTop + 'px' : mouse.startY + 'px';
        }
    }

    canvas.onmousedown = function (e) {
		if(element === null && drawing_mode == 1){
            mouse.startX = mouse.x;
            mouse.startY = mouse.y + global_scrollTop;
            element = document.createElement('span');
            element.className = shape_type + ' not-saved shape';
						element.style.display = 'inline-block';
            element.style.left = mouse.x + 'px';
            element.style.top = mouse.y + global_scrollTop + 'px';
            canvas.appendChild(element);
            canvas.style.cursor = "crosshair";
		}
	}

	canvas.onmouseup = function (e) {
        if (element !== null && drawing_mode == 1) {
            element.startHeight = $(element).height();
						$(element).html('<p><i class="fa fa-times remove-task">'+
															'</i></p>');
						element = null;
            canvas.style.cursor = "default";
						$('#main_canvas').css('pointer-events', 'none');
						drawing_mode = 0;

						angular.element('body').scope().endDraw();
        }
    }
}

function createShape( shapes ){
	var canvas = document.getElementById('main_canvas');

	$.each(shapes, function( index, shape ) {
		element = document.createElement('span');
		element.className = shape.type + ' shape';
		element.style.display = 'inline-block';
		element.style.left = shape.x - $(canvas).offset().left + 'px';
		element.style.top = shape.y - $(canvas).offset().top + 'px';
		element.style.width = shape.w + 'px';
		element.style.height = shape.h + 'px';
		$(element).html('<p><i class="fa fa-times remove-task">'+
											'</i></p>');
		canvas.appendChild(element);
	});
}

	function clearShapes(){
		$('.rectangle').remove();
		$('.circle').remove();
	}
