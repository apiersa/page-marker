//zmienne globalne dla calej aplikacji

var figures_on_screen = []; //potrzebna do kasowania figur przy zmianie strony w
                            //iframe np. klikniecie linka

var global_scrollTop = 0; //przesuniecie otrzymane od strony potomnej, uzywane do
                          //odrysowywania figur przy scrollowaniu

var load_notes = true;    //ustawienie tej flagi na false zatrzymuje odswiezanie
                          //zadan w petli

        $(document).ready(function(){
            var h1 = document.getElementById("right-box").offsetHeight;
            var h2 = h1-192;
            document.getElementById('no-see').style.height=h2+"px";
        });
        $(document).ready(function(){
            var h1 = document.getElementById("right-box").offsetHeight;
            var h2 = h1-192;
            document.getElementById('see').style.height=h2+"px";
        });

        <!-- Klikniecie strony -->
        $(document).ready(function() {
            $("#pages-tr").click(function () {
                $('#see').show();
                $('#see-emplo').hide();
                $('#no-see').hide();
            });
        });

        $(document).ready(function() {
            $(".employer").click(function () {
                toggleUsers();
            });
        });

		    $(document).ready(function() {
            $("#tr-hover").click(function () {
                togglePages();
            });
        });

        $(document).ready(function() {
            $(".emplo-tr").click(function () {
                $('#see').show();
                $('#no-see').hide();
                $('#see-emplo').hide();
            });
        });

        $(document).ready(function() {
            $("#sound-on").click(function () {
                $('#sound').toggle();
                document.getElementById('text').style.display = 'none';
            });
        });

        <!-- Rozpoczecie nowego zadania -->
        $(document).ready(function() {
            $("#text-on").click(function () {
                $('#text').toggle();
                if( $('#text').is(":visible") == 1){
                  angular.element('body').scope().newTag();
                  clearShapes();
                  drawing_mode = 1;
                }
                else {
                  angular.element('body').scope().cancelNote();
                }
                $('#sound').hide();
            });
        });

        <!-- Dobieranie zadan przy scrollowaniu -->
        $(document).ready(function() {
          $('#no-see').scroll( function (){
            if($(this).height() + $(this).scrollTop() >= $(this)[0].scrollHeight){
              angular.element('body').scope().increaseQuantity();
            }
          });
        });

        <!-- usuwanie ksztaltow iksem -->
        $(document).ready(function() {
          $(document).on('click', '.remove-task', function( event ) {
              var par_par = $(this).closest('span').remove();
          });
        });


function toggleUsers() {
	$('#see-emplo').show();
  $('#no-see').hide();
  $('#see').hide();
}

function togglePages() {
	   $('#no-see').show();
     document.getElementById('see').style.display = 'none';
     document.getElementById('see-emplo').style.display = 'none';
}




        $('#web_view').on("load", function () {
            if( figures_on_screen.length ){
              angular.element('body').scope().unselectNote();
              $.each(figures_on_screen, function( index, value ) {
                $(value).remove();
              });
              figures_on_screen = [];
            }
            else {
              figures_on_screen = $('.shape');
            }
        });


<!-------------------- OBSLUGA WIADOMOSCI OD STORNY POTOMNEJ ------------------> 
      window.addEventListener("message", function(e){
        if(e.data.type == 'path'){
          angular.element('body').scope().setSubpage(e.data.path);
        }
        else if(e.data.type == 'scroll'){
          global_scrollTop = e.data.scrollTop;
          redraw(e.data.scrollTop);
        }
      }, false);

  function changeOnLoadingIcon( obj ){
    load_notes = false;
    $(obj).empty();
    $(obj).html('<i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw margin-bottom"></i>');
  }

  function taskLoadingStart(){
    $('#task-description').hide();
    $('#save-task-link').hide();
    $('#text').append('<div id="loadedLayer" style="position:relative; \
                                   top: -10px;\
                                   left: 100px; \
                                   width: 100%; \
                                   height: 100%;"> \
                            <i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw margin-bottom"></i> \
                      </div>'
                    );
  }

  function taskLoadingEnd(){
    $('#loadedLayer').remove();
    $('#task-description').show();
    $('#save-task-link').show();
  }


  //  $(function() {
  //   $( "#speed" ).selectmenu();
  //
  //   $( "#files" ).selectmenu();
  //
  //   $( "#number" )
  //     .selectmenu()
  //     .selectmenu( "menuWidget" )
  //       .addClass( "overflow" );
  // });
  //       $(function() {
  //   $( "#speed1" ).selectmenu();
  //
  //   $( "#files" ).selectmenu();
  //
  //   $( "#number" )
  //     .selectmenu()
  //     .selectmenu( "menuWidget" )
  //       .addClass( "overflow" );
  // });
