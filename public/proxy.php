<?php
header("Content-Type: text/html; charset=utf-8");
header('Access-Control-Allow-Origin: *');
$url = $_GET['query'];
$domain_arr = explode('/', $url);
$domain = $domain_arr[0].'/'.$domain_arr[1].'/'.$domain_arr[2].'/';

$www = file_get_contents($url);

$phrases = array(
		0 => "/[^http:]\/\//",
		1 => "/src=[\"|\"\/][^http](.*)\"/",
		2 => "/src=['|'\/][^http](.*)'/",
		3 => "/href=[\"|\"\/][^http](.*)\"/",
		4 => "/href=['|'\/][^http](.*)'/"
);

$replacements = array(
	  0 => "\"http://",
		1 => "src=\"$domain$1\"",
		2 => 'src=\"$domain$1\"',
		3 => "href=\"$domain$1\"",
		4 => "href='$domain$1'"
);

$www = preg_replace($phrases, $replacements, $www);
echo $www;

?>
