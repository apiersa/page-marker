'use strict';

/* Directives */


var directives = angular.module('wiaDirectives', []);

directives.directive('selectOnClick', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('click', function () {
                this.select();
            });
        }
    };
});