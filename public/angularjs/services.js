'use strict';

/* Services */

angular.module('tagerApp')
.service('AppService', ['$http', '$q', function ($http, $q) {
			if(window.location.hostname == 'localhost'){
				var users_url = '/user';
				var url_prefix = '';
			}
			else {
				var users_url = '/demo/public/user';
				var url_prefix = '/demo/public/';
			}

			function getUsers() {
				var request = $http.get(users_url);

				return request.then(function (response) {
				return response.data;
				}, function (reason) {
				// jeśli wystąpił nieoczekiwany error
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

				// w innym przypadku użyj oczekiwanej wiadomości błędu
				return $q.reject(reason.data.message);
				});
			}

			function getPages( id ) {
				var request = $http.get(users_url + '/' + id + '/page');

				return request.then(function (response) {
				return response.data;
				}, function (reason) {
				// jeśli wystąpił nieoczekiwany error
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

				// w innym przypadku użyj oczekiwanej wiadomości błędu
				return $q.reject(reason.data.message);
				});
			}

			function getPage( user_id, page_id ) {
				var request = $http.get(users_url + '/' + user_id + '/page/' + page_id);

				return request.then(function (response) {
				return response.data;
				}, function (reason) {
				// jeśli wystąpił nieoczekiwany error
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

				// w innym przypadku użyj oczekiwanej wiadomości błędu
				return $q.reject(reason.data.message);
				});
			}

			function getNotes( user_id, page_id ) {
				var request = $http.get(users_url + '/' + user_id + '/page/' + page_id + '/note');

				return request.then(function (response) {
				return response.data;
				}, function (reason) {
				// jeśli wystąpił nieoczekiwany error
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

				// w innym przypadku użyj oczekiwanej wiadomości błędu
				return $q.reject(reason.data.message);
				});
			}

			function saveNote( user_id, page_id, id, note ){
				var request = $http.post(users_url + '/' + user_id + '/page/' + page_id + '/note/' + id, note);

				return request.then(function (response) {
					return response.data;
				}, function (reason) {
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

					return $q.reject(reason.data.message);
				});
			}

			function createNote( note ){
				var request = $http.post(url_prefix + 'saveNote/', note);

				return request.then(function (response) {
					return response.data;
				}, function (reason) {
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

					return $q.reject(reason.data.message);
				});
			}

			function deleteNote( id ){
				var request = $http.get(url_prefix + 'deleteNote/'+id);

				return request.then(function (response) {
					return response.data;
				}, function (reason) {
				if (!angular.isObject(reason.data) || !response.data.message)
					$q.reject("An unknown error occurred.");

					return $q.reject(reason.data.message);
				});
			}

			return ({
				getUsers: function () {
					return getUsers();
				},
				getPages: function ( id ) {
					return getPages( id );
				},
				getPage: function ( user_id, page_id ) {
					return getPage( user_id, page_id );
				},
				getNotes: function ( user_id, page_id ) {
					return getNotes( user_id, page_id );
				},
				saveNote: function ( user_id, page_id, id, note ) {
					return saveNote( user_id, page_id, id, note );
				},
				createNote: function ( note ) {
					return createNote( note );
				},

				deleteNote: function ( id ) {
					return deleteNote( id );
				}
			});
	} ]);


	angular.module('tagerApp')
	.service('WwwService', ['$http', '$q', function ($http, $q) {
		function openPage(addr) {
			var request = $http.put('/wwwservice', {"addr": addr});

			return request.then(function (response) {
			return response.data;
			}, function (reason) {
			// jeśli wystąpił nieoczekiwany error
			if (!angular.isObject(reason.data) || !response.data.message)
				$q.reject("An unknown error occurred.");

			// w innym przypadku użyj oczekiwanej wiadomości błędu
			return $q.reject(reason.data.message);
			});

		}

			return ({
				openPage: function ( addr ) {
					return openPage(addr);
				}
			})
	}]);
