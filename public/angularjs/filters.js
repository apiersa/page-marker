'use strict';

var coinrexFilters = angular.module('wiaFilters', []);
//
//coinrexFilters.filter('supportMessageSourceFilter', function() {
//	 return function(source){
//		 if(source=="S") return true;
//		 else return false;
//	 };
//	});
//
//coinrexFilters.filter('timestampToDateTimeFilter', function(){
//	return function(timestamp)
//	{
//		return new Date(timestamp * 1000);
//	};
//});
//
//

coinrexFilters.filter('subStr', function() {
	return function(a, b, c){
		if(c)
			return a.substr(b, c);
		return a.substr(b);
	};
});

coinrexFilters.filter('convertToDateFilter', function($filter) {
	var customDateFilterFn = $filter('date');
	return function(timestamp) {
		if (timestamp) {
			return customDateFilterFn(new Date(timestamp * 1000),
					'yyyy-MM-dd');
		}
		return null;
	};
});

coinrexFilters.filter('convertToTimeFilter', function($filter) {
	var customDateFilterFn = $filter('date');
	return function(timestamp) {
		if (timestamp) {
			return customDateFilterFn(new Date(timestamp * 1000),
					'HH:mm:ss');
		}
		return null;
	};
});

coinrexFilters.filter('unixToDateFilter', function($filter) {
	var customDateFilterFn = $filter('date');
	return function(timestamp) {
		if (timestamp) {
			return customDateFilterFn(new Date(timestamp * 1000),
					'yyyy-MM-dd');
		}
		return '-';
	};
});


coinrexFilters.filter('unixTohhmmFilter', function($filter) {
	var customDateFilterFn = $filter('date');
	return function(timestamp) {
		if (timestamp) {
			return customDateFilterFn(new Date(timestamp * 1000),
					'HH:mm');
		}
		return '-';
	};
});

coinrexFilters.filter('dateTohhmmFilter', function($filter) {
	var customDateFilterFn = $filter('date');
	return function(dateStr) {
		if (dateStr) {
			//var pattern = /(\d{2})\-(\d{2})\-(\d{4}) .(\d{2}):.(\d{2}):.(\d{2})+.(\d{2})/;
			//dateStr = dateStr.replace(pattern,'$1-$2-$3T$4-$5');
			/*
			alert(dateStr);
			var dt = new Date();		
			return customDateFilterFn(new Date(dateStr),
					'HH:mm');*/
			dateStr = dateStr.split(" ")[1];
			var timeStr = dateStr.split("+")[0];
			var hoursArr = timeStr.split(":");
			var hoursStr = hoursArr[0]+":"+hoursArr[1];
			return hoursStr;
		}
		return '-';
	};
});

coinrexFilters.filter('s2hhmmFilter', function($filter) {
	return function(sekundyOdpolnocy) {
		var sec = sekundyOdpolnocy % 60;
		sekundyOdpolnocy = (sekundyOdpolnocy - sec) / 60;

		var min = sekundyOdpolnocy % 60;
		sekundyOdpolnocy = (sekundyOdpolnocy - min) / 60;
		var hour = sekundyOdpolnocy;

		if(!hour) {
			return "";
			
		} else {
			return (hour < 10 ? '0'+hour : hour) + ":" + (min < 10 ? '0'+min : min);
		}

	};
});

coinrexFilters.filter('searchStopFilter', function($filter) {
	return function(input, searchText, AND_OR) {
		if (!searchText) {
			return input;
		}
		
	    var returnArray = [],
        // Split on single or multi space
    	splitext = searchText.toLowerCase().split(/\s+/),
        // Build Regexp with Logical AND using "look ahead assertions"
        regexp_and = "(?=.*" + splitext.join(")(?=.*") + ")",
        // Build Regexp with logicial OR
        regexp_or = searchText.toLowerCase().replace(/\s+/g, "|"),
        // Compile the regular expression
        re = new RegExp((AND_OR == "AND") ? regexp_and : regexp_or, "i");

	    for (var x = 0; x < input.length; x++) {
	        if (re.test(input[x].name)) returnArray.push(input[x]);
	    }
	    return returnArray;

	};
});

coinrexFilters.filter('searchNumberStopFilter', function($filter) {
	return function(input, searchText, AND_OR) {
		if (!searchText) {
			return input;
		}
		
	    var returnArray = [],
        // Split on single or multi space
    	splitext = searchText.toLowerCase().split(/\s+/),
        // Build Regexp with Logical AND using "look ahead assertions"
        regexp_and = "(?=.*" + splitext.join(")(?=.*") + ")",
        // Build Regexp with logicial OR
        regexp_or = searchText.toLowerCase().replace(/\s+/g, "|"),
        // Compile the regular expression
        re = new RegExp((AND_OR == "AND") ? regexp_and : regexp_or, "i");

	    for (var x = 0; x < input.length; x++) {
	        if (re.test(input[x].nr)) returnArray.push(input[x]);
	    }
	    return returnArray;

	};
});


//
// coinrexFilters.filter('haveAnyItemsFilter', function(){
//	    return function(input){
//	    	    if(input && input.length > 0){
//	    	      return true;
//	    	    } else {
//	    	      return false;
//	    	    }
//   };  
//});
//
//
//coinrexFilters.filter('coinrexCurrency', function($filter){
//	 var customNumberFilterFn = $filter('number');
//	    return function(number, currency){
//	    	var numTxt = customNumberFilterFn(number, 3);
//	    	
//	    	var zeros = numTxt.match(/[0]+$/g);
//	    	if (zeros != null) {
//	    		numTxt = numTxt.replace(/[0]+$/g, '<span class="cl-gray">' + zeros[0] + '</span>');
//	    	}
//	    	//console.log("numTxt:" + numTxt);
//	    	
//	    	return numTxt + ' ' + currency;
//    };  
//});
