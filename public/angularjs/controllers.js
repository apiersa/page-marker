'use strict';

/* Controllers */

var app_var = angular.module('tagerApp');

app_var.controller('mainController', ['$scope', '$interval', 'AppService', 'WwwService',
										function ($scope, $interval, AppService, WwwService) {
	$scope.users = [];
	$scope.selected_user = null;

	$scope.pages = [];
	$scope.selected_page = null;

	$scope.notes = [];
	$scope.selected_note = null;
	$scope.selected_note_name = '/';
	$scope.selected_note_id = -1;

	$scope.interval = null;

	$scope.quantity = 30;

	$scope.isNoteLoading = true;

	$scope.show_draw_tools = false;

	$scope.draw_tool = 'rectangle';

	$scope.searchPhrase = '';

	$scope.keep_empty_note = false;

	$scope.increaseQuantity = function (){
		$scope.quantity += 30;
		if($scope.quantity < $scope.notes.count){
			$scope.isNoteLoading = true;
		}
		else {
			$scope.isNoteLoading = false;
		}
	};

	$scope.runApp = function (){
		AppService.getUsers()
		    .then(function (result) {
			$scope.users = result;
			$scope.selected_user = result[0];
			$scope.getPages( $scope.selected_user.id, true )
			}, function (reason) {
			console.log("Nie można pobrać edycji!");
			});
	};

	$scope.getUsers = function (){
		AppService.getUsers()
		    .then(function (result) {
			$scope.users = result;
			}, function (reason) {
			console.log("Nie można pobrać edycji!");
			});
	};


	$scope.getNotesWithSelection = function ( user_id, page_id, note_id ){
		if( load_notes ){
		var selected_index = 0;
		AppService.getNotes( user_id, page_id )
		.then(function (result) {
			if( ! load_notes ){ return; }
			 $scope.notes = result[0].notes;
			 angular.forEach($scope.notes, function(member, index){
   	 					member.index = index;
							if(member.id == note_id){
								selected_index = index;
							}
			 });

			 		$scope.selectNote( selected_index );

			}, function (reason) {
			console.log("Nie można pobrać edycji!");
		});

		if($scope.quantity < $scope.notes.count){
			$scope.isNoteLoading = true;
		}
		else {
			$scope.isNoteLoading = false;
		}

		$scope.draw_tool = shape_type;
		}
	};

	$scope.getPages = function ( id, selectFirst ){
		AppService.getPages( id )
		.then(function (result) {
			$scope.pages = result;
			if(selectFirst){
				$scope.selected_page = $scope.pages[0];
				$scope.getNotes($scope.selected_user.id, $scope.selected_page.id, 0);
			}
		}, function (reason) {
			console.log("Nie można pobrać edycji!");
		});
	};

	$scope.refreshPage = function ( user_id, page_id ){
			AppService.getPage( user_id, page_id )
			.then(function (result) {
				$scope.selected_page = result;
				if( $scope.selected_page ) {
					$scope.selected_note_id = -1;
					$scope.getNotes( $scope.selected_user.id, $scope.selected_page.id, $scope.selected_note.id );
				}
			}, function (reason) {
				console.log("Nie można pobrać edycji!");
			});
		};

		$scope.refreshPageWithSelection = function ( user_id, page_id, note_id ){
				AppService.getPage( user_id, page_id )
				.then(function (result) {
					$scope.selected_page = result;
					if( $scope.selected_page ) {
						$scope.selected_note_id = -1;
						$scope.getNotesWithSelection( $scope.selected_user.id, $scope.selected_page.id, note_id );
					}
				}, function (reason) {
					console.log("Nie można pobrać edycji!");
				});
		};

	$scope.getNotes = function ( user_id, page_id, note_id, fillNotes ){
		if( fillNotes ) { $scope.keep_empty_note = false; load_notes = true; }
		if( load_notes ){
		var selected_index = 0;
		AppService.getNotes( user_id, page_id )
		.then(function (result) {
			if( ! load_notes ){ return; }
			 $scope.notes = result[0].notes;
			 angular.forEach($scope.notes, function(member, index){
   	 					member.index = index;
							if(note_id == member.id){
								selected_index = index;
							}
			 });
			 $scope.selectNote( selected_index );

			}, function (reason) {
			console.log("Nie można pobrać edycji!");
		});

		if($scope.quantity < $scope.notes.count){
			$scope.isNoteLoading = true;
		}
		else {
			$scope.isNoteLoading = false;
		}

		$scope.draw_tool = shape_type;
		}
	};

	$scope.getNotesNoReload = function ( user_id, page_id ){
		if( load_notes ){
		AppService.getNotes( user_id, page_id )
		.then(function (result) {
			if( ! load_notes ){ return; }
			 $scope.notes = result[0].notes;
			 angular.forEach($scope.notes, function(member, index){
   	 					member.index = index;
			 });

			}, function (reason) {
			console.log("Nie można pobrać edycji!");
		});

		if($scope.quantity < $scope.notes.count){
			$scope.isNoteLoading = true;
		}
		else {
			$scope.isNoteLoading = false;
		}

		$scope.draw_tool = shape_type;
		}
	};

	$scope.selectUser = function ( id ){
		$scope.selected_user = $scope.users[id];
		$scope.getPages( $scope.selected_user.id );
		load_notes = false;
		$scope.selected_page = null;
		$('#see').show();
		document.getElementById('no-see').style.display = 'none';
		document.getElementById('see-emplo').style.display = 'none';
	};

	$scope.selectPage = function ( index ){
		$scope.keep_empty_note = true;
		$scope.selected_page = $scope.pages[index];
		$scope.getNotes( $scope.selected_user.id, $scope.selected_page.id, 0, true );
		$scope.loadPage('/');

		togglePages();

	};

	var func_counter = 0;
	$scope.selectNote = function ( index ){
		if($scope.selected_note_id == $scope.notes[index].id){
			return;
		}
		$('#main_canvas').offset({top: 0});
		$scope.selected_note_id = $scope.notes[index].id;
		$scope.selected_note = $scope.notes[index];
		$scope.selected_note_name = $scope.selected_note.name;
		$scope.loadPage($scope.selected_note.name);
		var shape = JSON.parse($scope.selected_note.shape);
		createShape(shape);
		if(func_counter === 0){
			//odswiezanie listy co 3s
			$scope.interval = $interval( function() {
				$scope.getNotesNoReload ( $scope.selected_user.id, $scope.selected_page.id );
			}, 3000);
			func_counter++;
		}
	};

	$scope.unselectNote = function() {
		if($scope.selected_note.name != $scope.selected_note_name){
			$scope.selected_note = null;
			$scope.selected_note_id = null;
		}
	}

	$scope.endTask = function ( note, id ){
		changeOnLoadingIcon( $('td[data-id="' + id + '"]') );
		$("#audio").trigger('play');
		note.ended = 1;
		note.important = 0;
		AppService.saveNote( $scope.selected_user.id, $scope.selected_page.id, note.id, note )
		.then(function (result) {
			load_notes = true;
			$scope.refreshPage( $scope.selected_user.id, $scope.selected_page.id );
			$('.not-saved').remove();
		   }, function (reason) {
			console.log("Nie można pobrać edycji!");
		});
	};

	$scope.restartTask = function ( note, id ){
		changeOnLoadingIcon( $('td[data-id="' + id + '"]') );
		note.ended = 0;
		note.important = 0;
		AppService.saveNote( $scope.selected_user.id, $scope.selected_page.id, note.id, note )
		.then(function (result) {
			load_notes = true;
				$scope.refreshPage( $scope.selected_user.id, $scope.selected_page.id );
			$('.not-saved').remove();
		   }, function (reason) {
			console.log("Nie można pobrać edycji!");
		});
	};

	$scope.makeImportantTask = function ( note, id ){
		changeOnLoadingIcon( $('td[data-id="' + id + '"]') );

		note.important = note.important == 1 ? 0 : 1;
		AppService.saveNote( $scope.selected_user.id, $scope.selected_page.id, note.id, note )
		.then(function (result) {
			load_notes = true;
			$scope.refreshPage( $scope.selected_user.id, $scope.selected_page.id );
			$('.not-saved').remove();
		   }, function (reason) {
			console.log("Nie można pobrać edycji!");
		});
	};


	//obsluga www
	$scope.www_prefix = 'http://';
	$scope.url = '';

	$scope.loadPage = function ( name ) {
		clearShapes();
		$scope.url = $scope.www_prefix + $scope.selected_page.name + name;
		$scope.selected_note_name = name;
		$('#web_view')[0].src = $scope.url;
	};

	$scope.setSubpage = function ( name ) {
		$scope.selected_note_name = name;
	}

	$scope.newTag = function () {
		$scope.selected_note = null;
		$scope.selected_note_id = null;
		drawing_mode = 1;
		$('#main_canvas').css('pointer-events', 'auto');
		$('#main_canvas').css('cursor', 'crosshair');
	};

	$scope.addTag = function () {
		drawing_mode = 1;
		$scope.show_draw_tools = true;
		$('#main_canvas').css('pointer-events', 'auto');
		$('#main_canvas').css('cursor', 'crosshair');
		add_figure_to_note_mode = true;
	};

	$scope.endDraw = function () {
		$scope.show_draw_tools = false;
		$('#main_canvas').css('cursor', 'normal');
		$('.not-saved').removeClass('not-saved');
		if(add_figure_to_note_mode){
			changeOnLoadingIcon( $('td[data-id="' + $scope.selected_note.index + '"]') );
			$scope.saveTask(false);
			add_figure_to_note_mode = false;
		}
	};

	$scope.cancelDraw = function () {
		$('#main_canvas').css('pointer-events', 'none');
		drawing_mode = 0;
		$scope.show_draw_tools = false;
		$('#main_canvas').css('cursor', 'normal');
		$('.not-saved').remove();
		//$scope.endDraw();
	};

	$scope.cancelNote = function() {
		$('.not-saved').remove();
		$('#task-description').val('');
		$('#main_canvas').css('pointer-events', 'none');
		drawing_mode = 0;
	}

	$scope.saveTask = function( create_mode ) {
		//teoretycznie powinna być tylko jedna niezapisana notka
		var shapes = [];
		$('.rectangle').each(function() {
			var rect = {
				x: $(this).offset().left,
				y: $(this).offset().top + global_scrollTop,
				w: $(this).width(),
				h: $(this).height(),
				type: 'rectangle'
			};

			shapes.push(rect);
		});

		$('.circle').each(function() {
			var circle = {
				x: $(this).offset().left,
				y: $(this).offset().top + global_scrollTop,
				w: $(this).width(),
				h: $(this).height(),
				type: 'circle'
			};

			shapes.push(circle);
		});

		var note = {
			name: $scope.selected_note_name ? $scope.selected_note_name : '/',
			content: create_mode ? $('#task-description').val() : $scope.selected_note.content,
			shape: angular.toJson(shapes),
			page_id: $scope.selected_page.id,
			important: create_mode ? 0 : $scope.selected_note.important,
			ended: 0
		};
		if(create_mode){
			AppService.createNote( note )
			.then(function (result) {
					if(result == false){
						return;
					}
					else {
						$scope.refreshPageWithSelection( $scope.selected_user.id, $scope.selected_page.id, result );
						$('#text').toggle();
						$('#task-description').val('');
						$('#main_canvas').css('pointer-events', 'none');
						taskLoadingEnd();
					}
			   }, function (reason) {
				console.log("Nie można pobrać edycji!");
			});
	  }
		else {
			AppService.saveNote( $scope.selected_user.id, $scope.selected_page.id, $scope.selected_note.id, note )
			.then(function (result) {
					$('#main_canvas').css('pointer-events', 'none');
					load_notes = true;
					$scope.selected_note_id = -1;
					$scope.refreshPage( $scope.selected_user.id, $scope.selected_page.id );
			   }, function (reason) {
				console.log("Nie można pobrać edycji!");
			});
		}

	}

	$scope.deleteTask = function ( id, index ) {
		changeOnLoadingIcon( $('td[data-id="' + index + '"]') );
		AppService.deleteNote( id )
		.then(function (result) {
					clearShapes();
					$scope.selected_note_id = -1;
					load_notes = true;
					$scope.refreshPage( $scope.selected_user.id, $scope.selected_page.id );
			 }, function (reason) {
			console.log("Nie można pobrać edycji!");
		});
	}

	$scope.setTool = function ( tool ) {
		$scope.draw_tool = tool;
		shape_type = tool;
	}

	}]);
