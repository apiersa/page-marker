<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="tagerApp">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <!-- style.css -> Layout -->
    <link rel="stylesheet" type="text/css"
					href="<?= asset('css/style.css') ?>">

    <!-- draw.css -> Modul rysowania -->
    <link rel="stylesheet" type="text/css"
					href="<?= asset('css/draw.css') ?>">

    <!-- Ikony font-awesome -->
    <link rel="stylesheet" type="text/css"
					href="<?= asset('css/font-awesome.min.css') ?>">

    <!-- Biblioteki zewnetrzne: jQuery, Angular.js -->
    <script src="<?= asset('js/lib/jquery.js') ?>"></script>
    <script src="<?= asset('angularjs/lib/angular.min.js') ?>"></script>

    <title>Proghelper</title>
</head>

<!--------------------------- BODY SECTION ------------------------------------>

<body ng-controller="mainController">
    <div class="container">
        <div class="www_frame">
            <!-- element div, na ktorym odbywa sie rysowanie figur -->
            <div id="main_canvas"></div>
<!--Funkcja redraw (drawing.js) odpowiada za przesuwanie figur przy scroll... ->
<!-- TODO: do jQuery -->
            <div id="web_div">
              <!-- onscroll="redraw('web_div')" -->
<!-- element iframe, w ktorym wyswietlane sa tagowane strony www -->
                <iframe id="web_view"
												frameborder=0
												width="100%"
												height="100%"
												scrolling="yes"></iframe>
            </div>
        </div>
    </div>

<!--------------------------- Menu uzytkownika -------------------------------->
    <div class="right-box" id="right-box" ng-init="runApp()">

<!--------------------------- Aktualny user ----------------------------------->
        <div class="employer">
            <table class="employer-table">
                <tr ng-click="getUsers()">
										<!-- TODO: avatars -->
										<td class="img">
                        <img src="img/avatar-2.jpg" class="mega-image">
                    </td>

                    <td>
                        <p class="name">
                            <% selected_user.imie %>
                                <% selected_user.nazwisko %>
                        </p>
                        <p class="title">
                            <% selected_user.description %>
                        </p>
                    </td>

                    <td style="width:32px;">
                        <a href="#" class="drop-button employer-1" id="emplo">
                            <i class="fa fa-caret-down"></i>
                        </a>
                    </td>
                </tr>
            </table>
        </div>

<!-------------------------- Aktualnie wybrana strona ------------------------->
        <div class="domain">
            <table class="domain-table" cellspacing="0" cellpadding="0">
                <tr ng-hide="selected_page == null" id="pages-tr" ng-click="getPages( selected_user.id, false )">
                    <td class="top">
                        <p class="domain-link">
                            <a href="">
                                <% selected_page.name %>
                            </a>
                            <span ng-show="selected_page.note_important_count > 0"
																	class="number-warning">
																<% selected_page.note_important_count %>
														</span>
                        </p>
                        <p class="domain-link-bottom">
                            <span ng-show="selected_page.note_notended_count > 0">
                              <span class="number acti">
															  <% selected_page.note_notended_count %>
														  </span> aktywne
                            </span>

                          <span>
                            <span class="number lf-m">
															<% selected_page.note_count %>
														</span> wszystkie
                          </span>
                        </p>
                    </td>

                    <td style="width:34px;" class="top">
                        <a href="#" class="drop-button employer-2" id="click">
                            <i class="fa fa-caret-down"></i>
                        </a>
                    </td>
                </tr>
<!------------------------- Wyszukiwarka -------------------------------------->
                <tr>
                    <td colspan="2" class="domain-input-td">
                        <input type="text"
															 ng-model="searchPhrase"
															 class="domain-input"
															 placeholder="Przeszukaj...">
                        <a href="#" class="domain-ico">
                            <i class="fa fa-search"></i>
                        </a>
                    </td>
                </tr>
            </table>


<!----------------------------- Lista userow ---------------------------------->
            <div id="see-emplo" style="display:none;">
                <table class="employer-table-2" cellspacing="0" cellpadding="0">
                    <tr class="tr-hover-2"
												id="tr-hover-employer"
												ng-repeat="user in users | filter: { full_name: searchPhrase }">

                        <td class="img">
                            <img src="img/avatar-2.jpg" class="mega-image">
                        </td>

                        <td ng-click="selectUser($index)">
                            <p class="name">
                                <% user.imie %>
                                    <% user.nazwisko %>
                            </p>
                            <p class="title">
                                <% user.description %>
                            </p>
                        </td>
                    </tr>
                </table>
            </div>

<!----------------------------- Lista stron usera ----------------------------->
            <div id="see" style="display:none;">
                <table class="domain-table" cellspacing="0" cellpadding="0">
                    <tr class="tr-hover"
												id="tr-hover"
												ng-repeat="page in pages | filter: { name: searchPhrase }">

                        <td class="top" ng-click="selectPage($index)">
                            <p class="domain-link">
                                <a href="#">
                                    <% page.name %>
                                </a>
                                <span ng-show="page.note_important_count > 0"
                                      class="number-warning">

											<% page.note_important_count %>

								</span>
                            </p>
                            <p class="domain-link-bottom">
                              <span class="number acti">
                                  <% page.note_notended_count %>
                              </span> aktywne
                              <span class="number lf-m"><% page.note_count %></span>
                              wszystkie
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

<!------------------------------ Lista taskow ---------------------------------->

<!-- dzwiek odtwarzany po kliknieciu zielonego przycisku (zatwierdzenie taska) -->
        <audio id="audio" src="<?= asset('sounds/accept.wav') ?>"></audio>
        <div class="task" id="no-see" style="overflow: auto; height: 100%" onscroll="">
            <table class="task-table" cellspacing="0" cellpadding="0">
                <tr class="tr-hover-3"
                    ng-show="keep_empty_note == false"
										ng-click="selectNote(note.index)"
										id="tr-hover-3"
										ng-class="{'note-selected': note.id == selected_note.id}"
										ng-repeat="note in notes | limitTo:quantity | filter: { name: searchPhrase }">

                    <td ng-class="{'top': !note.ended}">
                        <p class="task-link">
                            <a>
                                <% note.name %>
                            </a>
                        </p>

                        <p class="task-link-bottom" ng-hide="note.id == selected_note.id">
                            <% note.content | limitTo:15 %>
                        </p>

<!----------------------- ONLY SELECTED NOTE ---------------------------------->
                        <p class="task-link-bottom"
                           style="max-width: 150px; word-wrap: break-word;"
                           ng-show="note.id == selected_note.id">
                            <% note.content %>
                        </p>

                        <p class="task-link-bottom">
                            <button ng-click="deleteTask( note.id, note.index )"
                                    style="background-color: #D33; /* TODO: refactoring */
														 border: none;
														 cursor:pointer;" ng-show="note.id == selected_note.id">

                                <i class="fa fa-times"></i>
                            </button>
                            <button ng-click="addTag(); $event.stopPropagation();"
                                    style="background-color: #AAA; /* TODO: refactoring */
							    					 border: none;
														 cursor:pointer;" ng-show="note.id == selected_note.id">

                                <i class="fa fa-pencil"></i>
                            </button>
                        </p>

                        <p ng-show="show_draw_tools && note.id == selected_note.id">
                            <button class="rectangle-btn"
                                    ng-class="{'selected-tool-btn' : draw_tool == 'rectangle'}"
                                    ng-click="setTool('rectangle')">

                                <i class="fa fa-square-o"></i>
                            </button>

                            <button class="circle-btn"
                                    ng-class="{'selected-tool-btn' : draw_tool == 'circle'}"
                                    ng-click="setTool('circle')">
                                <i class="fa fa-circle-thin"></i>
                            </button>

                            <button ng-click="cancelDraw()" style="background-color: #AAA; /* TODO: refactoring */
																				border: none;
																				cursor: pointer;">
                                <i class="fa fa-times"></i>
                            </button>
                        </p>
<!------------------------- END ONLY SELECTED NOTE ---------------------------->
                    </td>
                    <td data-id="<% note.index %>" ng-class="{'top': note.ended == 0, 'hidden': note.ended == 1}" class="right">
                        <i ng-show="note.important == 1 && note.ended == 0"
                           ng-click="makeImportantTask( note, note.index )"
                           class="fa fa-exclamation-circle warning"></i>
                        <i ng-show="note.important == 0 && note.ended == 0"
                           class="fa fa-exclamation-circle no-warning"
                           ng-click="makeImportantTask( note, note.index )">
												</i>
                        <a href="#">
                            <i class="fa fa-check-circle hidden accept"
                               ng-click="endTask(note, note.index)"
                               style="display:none;"></i>
                            <i ng-show="note.ended == 0" class="fa fa-circle accept look"></i>
                        </a>
                        <i ng-click="restartTask( note, note.index )"
                           ng-show="note.ended == 1"
                           class="fa fa-arrow-circle-o-up hidden"></i>
                        <i ng-show="note.ended == 1" class="fa fa-check-circle hidden accept"></i>
                    </td>
                </tr>

                <tr ng-show="isNoteLoading">
                    <td colspan="2" style="border:0;">
                        <center>
                            <p class="load-ico">
                              <i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw margin-bottom"
                                 style="margin-bottom:160px"></i>
                            </p>
                        </center>
                    </td>
                </tr>

            </table>
        </div>

<!------------------------ DOLNE MENU - Dodawanie taskow ---------------------->
        <div class="clear"></div>
        <div class="add-text" id="text" style="display:none;">
            <table class="add-text-table">
                <tr>
                    <td style="text-align: right;">
                        <button class="rectangle-btn" ng-class="{'selected-tool-btn' : draw_tool == 'rectangle'}" ng-click="setTool('rectangle')">
                            <i class="fa fa-square-o"></i>
                        </button>

                        <button class="circle-btn" ng-class="{'selected-tool-btn' : draw_tool == 'circle'}" ng-click="setTool('circle')">
                            <i class="fa fa-circle-thin"></i>
                        </button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="text-sound-page">
                            <% selected_note_name %>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <textarea rows="3" id="task-description" class="text-task" placeholder="opisz zadanie"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a id="save-task-link" href="#" ng-click="saveTask(true)" onclick="taskLoadingStart()" class="bbc" style="margin-left: 14px;">

                            <i class="fa fa-save"></i>
                        </a>

                    </td>
                </tr>
            </table>
        </div>
        <div class="bottom-buttons">
            <table class="buttons" cellspacing="0" cellpadding="0">
                <tr>
                    <td><a href="#" id="text-on"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script src="<?= asset('js/events.js') ?>"></script>
    <!-- ANGULAR.JS -->
    <script src="<?= asset('angularjs/app.js') ?>"></script>
    <script src="<?= asset('angularjs/services.js') ?>"></script>
    <script src="<?= asset('angularjs/controllers.js') ?>"></script>
    <!---------------->
    <script src="<?= asset('js/drawing.js') ?>"></script>

</body>
</html>
