<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $faker = Faker\Factory::create();
	   foreach(range(1, 5) as $v){
		   DB::table('users')->insert([
				'imie' => $faker->name,
				'nazwisko' => $faker->lastName,
				'description' => $faker->word
			]);
	   }
    }
}
