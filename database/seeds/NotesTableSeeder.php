<?php

use Illuminate\Database\Seeder;

class NotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $faker = Faker\Factory::create();
		foreach(range(1, 40) as $v){
		   DB::table('notes')->insert([
				'content' => $faker->paragraph,
				'shape' => 'RECT',
				'page_id' => 1,
				'important' => 0,
				'ended' => 0
			]);
	   }
    }
}
